import logging
import os
import time

from colorama import Fore
import sys
import cv2
import numpy as np

import bluvar.utils

from constants import YOLOV5_BASE_PATH

__DIR__ = os.path.dirname(os.path.realpath(__file__))


def is_yolov5_cloned():
    return os.path.isfile(os.path.join(YOLOV5_BASE_PATH, "detect.py"))


def add_yolov5_dir_to_path():
    import torch

    if not is_yolov5_cloned():
        torch.hub.load(
            "ultralytics/yolov5",
            "custom",
            path=os.path.join(__DIR__, "assets/yolov5s.engine"),
            force_reload=True,
        )
    if not is_yolov5_cloned():
        print(Fore.RED + "Failed to clone yolov5")
        exit(1)

    sys.path.append(YOLOV5_BASE_PATH)


def get_device():
    import utils.torch_utils

    # CUDA device
    return utils.torch_utils.select_device()


def load_tensorrt_model():
    import models.common
    import utils.torch_utils

    model = models.common.DetectMultiBackend(
        weights=os.path.join(__DIR__, "assets/yolov5s.engine"),
        device=get_device(),
        dnn=False,
        fp16=True,  # for speed but reduce accuracy
    )
    img_sz = utils.general.check_img_size(imgsz=[640, 640], s=model.stride)
    model.warmup(imgsz=(1, 3, *img_sz))

    return model


def load_pytorch_model():
    import models.common
    import utils.torch_utils

    model = models.common.DetectMultiBackend(
        weights=os.path.join(__DIR__, "assets/yolov5s.pt"),
        device=get_device(),
        dnn=False,
        fp16=True,  # for speed but reduce accuracy
    )
    img_sz = utils.general.check_img_size(imgsz=[640, 640], s=model.stride)
    model.warmup(imgsz=(1, 3, *img_sz))

    return model


def benchmarks_method(name, model):
    import torch

    total_t = 0
    run_time = 1000

    logging.getLogger("yolov5").setLevel(logging.CRITICAL)
    print(Fore.WHITE + "---\nRunning method: {}...".format(name))

    for i in bluvar.utils.progress_bar(
        range(0, run_time),
        prefix="Progress:",
        suffix="Complete",
        length=50,
    ):
        img = cv2.imread(os.path.join(__DIR__, "assets/images/1654717264874_640.png"))
        t0 = time.time()
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = img.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
        img = np.ascontiguousarray(img)

        im = torch.from_numpy(img).to(get_device())
        im = im.half() if model.fp16 else im.float()
        im /= 255
        if len(im.shape) == 3:
            im = im[None]
        model(im, augment=False, visualize=False)
        t1 = time.time() - t0
        total_t += t1

    ms = (total_t / run_time) * 1000
    print(
        Fore.GREEN
        + "[{}] average time: {} ms ~{} fps".format(name, ms, int(1000/ms))
        + Fore.RESET
    )
    logging.getLogger("yolov5").setLevel(logging.DEBUG)


def main():
    pytorch_model = load_pytorch_model()
    tensorrt_model = load_tensorrt_model()

    benchmarks_method("Pytorch", pytorch_model)
    benchmarks_method("TensorRT", tensorrt_model)


if __name__ == "__main__":
    # For pycharm development
    # Add: YOLOV5_BASE_PATH to Settings/Project v4nl0r4nt-4im/Project structure -> Add content root
    # to hide import error
    # print(YOLOV5_BASE_PATH)
    add_yolov5_dir_to_path()
    main()
