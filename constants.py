from pathlib import Path
import os

YOLOV5_BASE_PATH = os.path.join(
    Path.home(),
    ".cache/torch/hub/ultralytics_yolov5_master",
)
