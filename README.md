## Installation

1. Install Graphics driver: https://docs.nvidia.com/deeplearning/cudnn/archives/cudnn-850/install-guide/index.html#installdriver-windows
2. Install CUDA toolkit: https://docs.nvidia.com/deeplearning/cudnn/archives/cudnn-850/install-guide/index.html#installcuda-windows
3. Install Zlib: https://docs.nvidia.com/deeplearning/cudnn/archives/cudnn-850/install-guide/index.html#install-zlib-windows
4. Install CUDA dnn: https://docs.nvidia.com/deeplearning/cudnn/archives/cudnn-850/install-guide/index.html#download-windows
5. Install Tensorrt: https://docs.nvidia.com/deeplearning/tensorrt/install-guide/index.html#installing-zip
6. Install requirements:

```shell
pip install -r https://raw.githubusercontent.com/ultralytics/yolov5/master/requirements.txt
pip install requirements.txt
```

7. Intall torch with CUDA support: https://pytorch.org/get-started/locally/

## Run docter

If you see 'Everything is fine' -> God bless you. You've been through so much suffering.

```shell
python docter.py
```

## Run benchmarks on your machine

```shell
python benchmarks_detect_methods.py
```

## Results
```shell
# My PC Quadro RTX 4000, 8192MiB
Running method: Pytorch...
Progress: |██████████████████████████████████████████████████| 100.0% Complete
[Pytorch] average time: 8.216144561767578 ms ~121 fps
---
Running method: TensorRT...
Progress: |██████████████████████████████████████████████████| 100.0% Complete
[TensorRT] average time: 5.8698410987854 ms ~170 fps
```