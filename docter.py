import subprocess
from colorama import init, Fore
import os

from constants import YOLOV5_BASE_PATH

__DIR__ = os.path.dirname(os.path.realpath(__file__))


def sample_detect():
    try:
        print(Fore.WHITE + "Checking TensorRT...")
        import torch

        # Model
        force_reload = True
        if os.path.isfile(os.path.join(YOLOV5_BASE_PATH, "detect.py")):
            force_reload = False

        model = torch.hub.load(
            "ultralytics/yolov5",
            "custom",
            path=os.path.join(__DIR__, "assets/yolov5s.engine"),
            force_reload=force_reload,
        )

        # Image
        im = "https://ultralytics.com/images/zidane.jpg"

        # Inference
        results = model(im)

        print(results.pandas().xyxy[0])
        print(Fore.GREEN + "yolov5 TensorRT inference: OK")
    except:
        print(Fore.RED + "Failed to using TensorRT inference the sample")
        return False

    return True


def check_cuda():
    try:
        print(Fore.WHITE + "Checking CUDA version...")
        out = subprocess.run("nvcc --version", stdout=subprocess.PIPE)
        lines = out.stdout.decode("utf-8").strip().split("\n")
        last_line = lines[-1]
        build = last_line.split("/")[0]
        ver = build.split(" ")[1]
        ver_number = ver.split(".")
        major = int(ver_number[0].split("_")[-1])
        minor = int(ver_number[1])

        if not (major >= 11 and minor >= 3):
            print(Fore.RED + "NVIDIA CUDA Toolkit minimum required version: 11.3")
            return False
        else:
            print(Fore.GREEN + "NVIDIA CUDA Toolkit version: {}".format(ver))
    except:
        print(Fore.RED + "Missing NVIDIA CUDA Toolkit in your system PATH")
        print(
            "Link: https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/index.html"
        )
        return False
    #
    print(Fore.WHITE + "Checking torch version...")
    import torch

    if not torch.cuda.is_available():
        print(Fore.RED + "Torch is not allowed to use GPU")
        print("Link: https://pytorch.org/get-started/locally/")
        return False
    else:
        if ver.count("{}".format(torch.version.cuda)) == 0:
            print(
                Fore.YELLOW
                + "Torch is using a CUDA version different from than computer version."
            )
            print("Torch is using CUDA: {}".format(torch.version.cuda))
            print("Computer is using CUDA: {}".format(ver))
        print(Fore.GREEN + "Torch GPU: OK")
    return True


if __name__ == "__main__":
    init()
    dockers = [check_cuda, sample_detect]

    for docker in dockers:
        if not docker():
            exit(1)

    print(Fore.GREEN + "---")
    print("Everything is fine")
